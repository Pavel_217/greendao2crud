package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyGenerator {

public static void main(String[]args)throws Exception{

    Schema shema=new Schema(1,"com.pavelwinter.greendao2crud.db");

    Entity noteTable=shema.addEntity("Note");

    noteTable.addIdProperty();

    noteTable.addStringProperty("Name");
    noteTable.addIntProperty("Age");


    new DaoGenerator().generateAll(shema, "./app/src/main/java");

}
}
