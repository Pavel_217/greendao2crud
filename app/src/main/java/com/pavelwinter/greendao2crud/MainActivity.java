package com.pavelwinter.greendao2crud;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.pavelwinter.greendao2crud.db.DaoSession;
import com.pavelwinter.greendao2crud.db.Note;
import com.pavelwinter.greendao2crud.db.NoteDao;

import java.util.List;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.QueryBuilder;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {


    private NoteDao mNoteDao;
    DaoSession daoSession;

    List<Note>noteList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       daoSession  = ((App)getApplication()).getDaoSession();

        mNoteDao=daoSession.getNoteDao();

        setContentView(R.layout.activity_main);





      //  createNote("Alex",30);
      //  upgradeNote("Alex","Michail");

      //  deleteNote("Michail");

       // readNote();
       /* readNote();




        readNote();
*/



    }



/**creates a new note in table Note*/
    void createNote(String name,int age){

        Note note=new Note(null,name,age);
        mNoteDao.insert(note);

    }


    void readNote(){

        QueryBuilder<Note> qb = daoSession.queryBuilder(Note.class);
        noteList = qb.orderAsc(NoteDao.Properties.Id).list();

        if(noteList!=null) {

            for (int i = 0; i < noteList.size(); i++) {

               Log.d("READ", noteList.get(i).getName());


                Timber.d("FROM READING");
            }
        }else{ Log.d("READ","The table has no notes");
        }
    }




    void upgradeNote(String nameBefore,String nameAfter){

        QueryBuilder<Note>noteQueryBuilder=daoSession.queryBuilder(Note.class);
        noteQueryBuilder.where(NoteDao.Properties.Name.eq(nameBefore));

        Note tempNote=noteQueryBuilder.list().get(0);

         tempNote.setName(nameAfter);

        mNoteDao.update(tempNote);

    }

    void deleteNote(String name){

        DeleteQuery<Note> query = daoSession.queryBuilder(Note.class)
                .where(NoteDao.Properties.Name.eq(name))
                .buildDelete();
        query.executeDeleteWithoutDetachingEntities();


    }

}
