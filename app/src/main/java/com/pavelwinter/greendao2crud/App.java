package com.pavelwinter.greendao2crud;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.facebook.stetho.Stetho;
import com.pavelwinter.greendao2crud.db.DaoMaster;
import com.pavelwinter.greendao2crud.db.DaoSession;

import timber.log.Timber;

/**
 * Created by newuser on 13.09.2017.
 */

public class App extends Application {

    DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();





        Timber.plant(new Timber.DebugTree());


        DaoMaster.DevOpenHelper masterHelper = new DaoMaster.DevOpenHelper(this,"myDb", null); //create database db file if not exist
        SQLiteDatabase db = masterHelper.getWritableDatabase();  //get the created database db file
        DaoMaster master = new DaoMaster(db);//create masterDao
        daoSession=master.newSession(); //Creates Session session


        // Create an InitializerBuilder
        Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);

        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );

        // Enable command line interface
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );

        // Use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer = initializerBuilder.build();

        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);

    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}